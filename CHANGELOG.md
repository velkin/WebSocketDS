# История изменений
 - Добавлен метод авторизации "permission_www", при этом методе производится только авторизация, аутентификация проводится автоматически, при любых введённых значениях "login" и "password".
 - Удалена переменная cache из WSThread_*, в которой хранилось последнее сообщение для клиентов. При использовании подписки на события в серверном режиме вызывало системную ошибку "corrupted double-linked"
 - Удалён атрибут JSON из основного танго-модуля
 - Исправлена поздняя отписка от событий в деструкторе ~EventSubscr()
 - Поправлено чтение IP-клиента при проксировании. Считывается дополнительно "X-Forwarded-For"
 - Добавлен дополнительный запрос на проверку статуса подключённого пользователя.
 - Убран метод аутентификации USERANDIDENT3, перенесён в USERANDIDENT с возможностью смены пользователя.
 - Добавлена возможность аутентификации после соединения, а также возможность смены текущего пользователя.
 - В атрибуты типа "written" добавляется информация по set (значению установленному в записываемый атрибут)
 - Добавлена возможность смены имени методов для авторизации и аутентицфикации

### 0.9.5 (23.10.2017)
 - Добавлено клиентное чтение атрибутов для групп девайсов
 - Добавлена подписка на события во всех режимах 
 - Добавлена возможность добавления группы в таймер
 - Поправлены скрипты для тестинга в директории  scripts_for_test
 - Поправлено удаление pipe со списка прослушиваемых при тайминге. До этого удалялись только атрибуты.
 - `"type_req"` для возвращаемых данных из таймера изменён на `"from_timer"` (был `"attribute_client"`)
 - В JSON-запросе для чтения клиентного чтения с атрибутов и pipe, изменён ключ `"pipes"` на `"pipe"`
 - В JSON-ответе, при клиентном чтении с атрибутов и pipe, данные с атрибутов и pipe помещены в общий блок с ключом `"data"`. Изначально данные из pipe находились в отдельном блоке с ключом `"pipe"`.
 - Добавилась возможность записи атрибутов. При записи также проходит авторизация.
 - Для записываемых атрибутов, выводятся только READ-данные. Раньше выводилось и READ и Write
 - Добавлен english README.md 

### 0.9.4 (27.06.2017)

 - Изменён вывод по умолчанию JSON-update для атрибутов. Удалены значения qual и time
 - Изменён json-формат запросов
 - Произведена реструктуризация кода
 - Поправлено отправление в журнал статуса выполнения. Изначально в качестве статуса отправлялся результат разрешения на выполнение
 - Классы перемещены в директорию wstangoproc
 - Поправлены методы авторизации и аутентификации
 - Добавлен новый метод авторизации USERANDIDENT2, проводящий авторизацию уже после подключения при запросе в два этапа.
 - Добавлен новый метод авторизации USERANDIDENT3, проводящий авторизацию уже после подключения при запросе в один этап.
 - Добавлены структуры для храниния данных о подключенном пользователе.

 - Добавлена возможность подключения обновления со стороны клиента.
 - Добавлена возможность управления клиентом приходящими данными, куда входит контроль над перечнем девайсов с перечисленными pipe attributes, а также периодом их обновления.
 - Измененены некоторые сообщения об ошибках.
 - Добавлена возможность включения в список атрибутов всех имеющихся в девайсе, добавив `__all_attrs__` в перечень атрибутов.
 - Добавлены режимы работы танго-сервера. Режим по умолчанию SERVER
 - Реализованы клиентные режимы работы танго модуля. Используются для управления клиентом видом обновляемой информации, также запуском команд и чтением атрибутов и pipe с указаных клиентом танго-устройств.
 - Мьютекс при отправлении сообщений, а также при закрытии и открытии соединения, не используется в клиентных режимах
 - Убрана жёсткая перегрузка сервера, при превышении ResetTimestampDifference, если не используется серверный режим
 - В случае ошибки при попытке выполнить команду с бинарным argout, отправляется JSON-сообщение в текстовом, а не бинарном формате.
 
 - Добавлено свойство Mode в Property. Управляет режимом работы.
 - Тип свойства Options заменено на array strings.

### 0.9.3 (28.03.2017)
 - Поправлен формат вывода JSON в случае ошибки
 - Добавлена возможность чтения с PIPE танго-девайсов в коммандном режиме. Имя PIPE указывается JSON-input
 - Добавлена возможность чтения с одного PIPE танго-девайсов в атрибутном режиме (добавляется к JSON-output для атрибутов). Имя PIPE указывается в property PipeName

 - Добавлена возможность соединения с группами танго-устройств. Соответсвующее свойство имени девайса должно быть указано в шаблонном формате.
 - Выполнение команд для групп возможно как для всей группы, так и для отдельных девайсов из группы.
 - Добавлена возможность выполнения команд с бинарным возвращаемым значением Tango::DEVVAR_CHARARRAY.
 - Убраны включение журналирования и изменения типа аутентификации через #define
 - Добавлен property Options, для выставления дополнительных опций
 - В список возможных значений для Options включены group (использования групп устройств), uselog (включения журналирования запускаемых команд), tident (изменение типа аутентификации)
 - Убран std::setprecision(5) по умолчанию.
 - Добавлена возможность установления периодичности вывода значений для атрибутов.

### 0.9.2 (02.11.2016)
 - Добавлена возможность выставления флагов std::fixed std::scientific для выходных значений команд типа с плавающей запятой
 - поправлен выводимый json-формат ошибок, атрибутов и результатов запуска команд
 - Добавлена возможность изменения точности (precision) для читаемых атрибутов типа с плавающей запятой. По умолчанию значение точности 5
 - Добавлена возможность выставления флагов std::fixed std::scientific для читаемых атрибутов типа с плавающей запятой
 - Перенесён под #ifdef TESTFAIL метод  sendLogToFile(), который запускался при принудительной перезагрузке девайс-сервера
 - Исправлено выставление state и status при инициализации сервера на ON или FAULT. До этого выставлялось UNKNOWN.
 - Добавлена реинициализация списка команд, если размер списка равен нулю, при имеющихся в property командах.
 - Добавлено свойство девайса MaxNumberOfConnections, задающее максиальное число подключённых клиентов
 - Добавлено свойство девайса MaximumBufferSize, задающее максимальный размер буфера для каждого соединения в КиБ
 - Добавлено свойство девайса ResetTimestampDifference
 - Добавлены скалярные атрибуты TimestampDiff и NumberOfConnections
 - Исправлено закрытие соединения со стороны сервера, при исключениях.
 - Отключён запуск метода CheckPoll, при выставленном device state = OFF. До этого, после минуты нахождения в статусе OFF, происходила перезагрузка девайс сервера.

### 0.9.1 (05.10.2016)
 - Добавлена возможность использования метода USERANDIDENT для идентификации пользователя
 - Удалена проверка пользователя при чтении атрибутов

### 0.9.0 (08.09.2016)
 - Добавлена возможность выполнения методов на прослушиваемом танго девайс-сервере
 - Добавлена возможность использования защищённого wss соединения
 - Добавлена возможность записи в журналы при запуске методов на прослушиваемом сервере
 - Добавлена принудительная перезагрузка WS танго-сервера, если poll sleeping превышает 60 секунд
 - Добавлены перехваты исключений при обрыве соединения и ошибках отправления
